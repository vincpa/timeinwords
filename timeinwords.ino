#include <SPI.h>        //SPI.h must be included as DMD is written by SPI (the IDE complains otherwise)
#include <DMD.h>        //
#include <TimerOne.h>   //
#include "SystemFont5x7.h"
#include "Arial_black_16.h"

//Fire up the DMD library as dmd
#define DISPLAYS_ACROSS 1
#define DISPLAYS_DOWN 1
DMD dmd(DISPLAYS_ACROSS, DISPLAYS_DOWN);

long secondsFromMidnight = 0;
int interuptCounter = 0;
long buttonPressedTimeOut = 0;
long buttonPressedTimeCount = 0;
int timerSpinUp = 1;
int switchPin = 3;
boolean buttonPressed = false;


void increaseMinutes() {
  
  if (!buttonPressed) {
      buttonPressedTimeCount = 0;
      timerSpinUp = 1;
  }
  
  if (buttonPressed && (buttonPressedTimeOut == 0 || buttonPressedTimeOut == -1)) {
       
        buttonPressedTimeOut=1; 
        
        secondsFromMidnight -= (secondsFromMidnight % 60);
        
        buttonPressedTimeCount++;
        if (buttonPressedTimeCount > 5 && timerSpinUp < 5) {
           timerSpinUp++; 
        }
        if (buttonPressedTimeCount > 15 && timerSpinUp < 15) {
           timerSpinUp++; 
        }

        secondsFromMidnight += (60 * timerSpinUp); //* timerSpinUp);
        drawScreen();
  }
  if (buttonPressedTimeOut>0) buttonPressedTimeOut++;
  
  if (buttonPressedTimeOut == 50) {
      buttonPressed = false;
      buttonPressedTimeOut = 0;
  }
 }




void timerInterupt()
{ 
  increaseMinutes();
  interuptCounter++;

  determineSecond();
  
  dmd.scanDisplayBySPI();
  /* The initializer is set to 5000 microseconds. 1,000,000 is 1 second 
     so 200 = 1,000,000 / 5000. Execute every second! */
}

void determineSecond() {

  if (interuptCounter == 200) {
     interuptCounter=0;
     drawScreen();
  
    secondsFromMidnight++; 
    if (secondsFromMidnight >= 86400){
      secondsFromMidnight = 0;
    }
  }  
}

void drawScreen() {
  
    int hours = secondsFromMidnight / 3600;
    int minutes = (secondsFromMidnight % 3600) / 60;
    int seconds = secondsFromMidnight - ((hours * 60 * 60) + (minutes * 60));

    char hrs[4];
    char mins[4];
    char secs[4];

    //Serial.println("Hrs: " + String(hours) +  " Min: " + String(minutes) +  " Sec:" + String(seconds) + " Ticks: " + String(i));
  
    formatTimeDigits(hrs, hours);
    formatTimeDigits(mins, minutes);
    formatTimeDigits(secs, seconds);
    
    dmd.drawString(0, 0, hrs, 3, GRAPHICS_NORMAL );
    dmd.drawString(16, 0, mins, 2, GRAPHICS_NORMAL );
    dmd.drawString(0, 8, secs, 2, GRAPHICS_NORMAL );

    dmd.drawFilledBox(13 + (seconds+9)%10, 8, 17 + (seconds+9)%10, 9, GRAPHICS_NOR);
    dmd.drawFilledBox(13 + seconds%10, 8, 17 + seconds%10, 9, GRAPHICS_NORMAL);

    dmd.drawFilledBox(13,10,22,15,GRAPHICS_NOR);
       
    int secMod = seconds%10;
    dmd.drawFilledBox(13+secMod,10,13+secMod,10,GRAPHICS_NORMAL);
    
    int secDiv = seconds/10;
    dmd.drawFilledBox(13+secDiv,11,13+secDiv,11,GRAPHICS_NORMAL);
    
    int minMod = minutes%10;
    dmd.drawFilledBox(13+minMod,12,13+minMod,12,GRAPHICS_NORMAL);    
    
    int minDiv = minutes/10;
    dmd.drawFilledBox(13+minDiv,13,13+minDiv,13,GRAPHICS_NORMAL);
    
    int hrsMod = hours%10;
    dmd.drawFilledBox(13+hrsMod,14,13+hrsMod,14,GRAPHICS_NORMAL);
    
    int hrsDiv = hours/10;
    dmd.drawFilledBox(13+hrsDiv,15,13+hrsDiv,15,GRAPHICS_NORMAL);
}

void setup() {
   
  Serial.begin(9600);
   
   Timer1.initialize( 5000 );           //period in microseconds to call timerInterupt. Anything longer than 5000 (5ms) and you can see flicker.
   Timer1.attachInterrupt( timerInterupt );   //attach the Timer1 interrupt to updateScreen which goes to dmd.scanDisplayBySPI()

   //clear/init the DMD pixels held in RAM
   dmd.clearScreen( true );   //true is normal (all pixels off), false is negative (all pixels on)
   dmd.selectFont(System5x7);
   
   pinMode(switchPin, INPUT);
}

void formatTimeDigits(char strOut[4], int num)
{
  strOut[0] = '0' + (num / 10);
  strOut[1] = '0' + (num % 10);
  strOut[2] = ':';
  strOut[3] = '\0';
}

int freeRam () {
  extern int __heap_start, *__brkval; 
  int v; 
  return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval); 
}

void loop() {
  // put your main code here, to run repeatedly: 
  
    if (digitalRead(switchPin) == HIGH)
    {
      dmd.drawFilledBox(30,15,30,15,GRAPHICS_NORMAL);
      //i = i+60;
      buttonPressed = true;
    }
    else
    {
      dmd.drawFilledBox(30,15,31,15,GRAPHICS_NOR);
    }
}
